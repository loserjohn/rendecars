
const defSet = {
	devEnv:true,  //是否是测试环境
	dev_appid: 'wx09daee2f47e178aa', //测试环境appid
	dev_redirect: 'http://test.fjdmll.com',  //测试环境h5授权回调地址
	pro_appid: 'wxe7812db19ae5b524', //正式环境appid
	pro_redirect: 'http://shop.fjdmll.com',  //正式环境h5授权回调地址
}
 
/*  当前常用配置*/
 const SET = {
	appid:defSet.devEnv?defSet.dev_appid:defSet.pro_appid, //appid
	redirect: defSet.devEnv?defSet.dev_redirect:defSet.pro_redirect,  //h5授权回调地址
	tokenName:'rent_token', //储存的token名字
	opIdName:'rent_opendid',
	// baseUrl:'',
	// mainUrl:''
	baseUrl:defSet.devEnv?'http://zz.fjdmll.com:7000':'http://zz.fjdmll.com:7000',
	mainUrl:defSet.devEnv?'http://zz.fjdmll.com':'http://zz.fjdmll.com',
	// baseUrl:defSet.devEnv?'http://test.fjdmll.com/Data':'http://shop.fjdmll.com/Data',
	// mainUrl:defSet.devEnv?'http://test.fjdmll.com':'http://shop.fjdmll.com'
}

export default SET