export const formatLocation = (longitude, latitude) => {
	if (typeof longitude === 'string' && typeof latitude === 'string') {
		longitude = parseFloat(longitude)
		latitude = parseFloat(latitude)
	}

	longitude = longitude.toFixed(2)
	latitude = latitude.toFixed(2)

	return {
		longitude: longitude.toString().split('.'),
		latitude: latitude.toString().split('.')
	}
}


export const IsPtInPoly = (aLat, aLon, pointList) => {
	/* 
	:param aLon: double 经度 
	:param aLat: double 纬度 
	:param pointList: list [{latitude: 22.22, longitude: 113.113}...] 多边形点的顺序需根据顺时针或逆时针，不能乱 
	
	*/
	var iSum = 0
	var iCount = pointList.length

	if (iCount < 3) {
		return false
	}
	for (var i = 0; i < iCount; i++) {
		var pLat1 = pointList[i].latitude
		var pLon1 = pointList[i].longitude
		if (i == iCount - 1) {
			var pLat2 = pointList[0].latitude
			var pLon2 = pointList[0].longitude
		} else {
			var pLat2 = pointList[i + 1].latitude
			var pLon2 = pointList[i + 1].longitude
		}
		if (((aLat >= pLat1) && (aLat < pLat2)) || ((aLat >= pLat2) && (aLat < pLat1))) {
			if (Math.abs(pLat1 - pLat2) > 0) {
				var pLon = pLon1 - ((pLon1 - pLon2) * (pLat1 - aLat)) / (pLat1 - pLat2);
				if (pLon < aLon) {
					iSum += 1
				}
			}
		}
	}
	if (iSum % 2 != 0) {
		return true
	} else {
		return false
	}
}

/*计算两点距离*/
export const getDistance = (lat1, lng1, lat2, lng2) => {
	lat1 = lat1 || 0;
	lng1 = lng1 || 0;
	lat2 = lat2 || 0;
	lng2 = lng2 || 0;

	var rad1 = lat1 * Math.PI / 180.0;
	var rad2 = lat2 * Math.PI / 180.0;
	var a = rad1 - rad2;
	var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
	var r = 6378137;
	var distance = r * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad1) * Math.cos(rad2) * Math
		.pow(Math.sin(b / 2), 2)));

	return distance;
}



export const points = [{
		longitude: 119.356882,
		latitude: 25.999342
	}, {
		longitude: 119.371731,
		latitude: 26.013305
	}, {
		longitude: 119.382116,
		latitude: 26.028885
	},
	{
		longitude: 119.365766,
		latitude: 26.051018
	},
	{
		longitude: 119.372117,
		latitude: 26.069985
	},
	{
		longitude: 119.370229,
		latitude: 26.096658
	},
	{
		longitude: 119.364907,
		latitude: 26.103904
	},
	{
		longitude: 119.355466,
		latitude: 26.105291
	},
	{
		longitude: 119.321134,
		latitude: 26.117006
	},
	{
		longitude: 119.295041,
		latitude: 26.121938
	},
	{
		longitude: 119.271009,
		latitude: 26.124096
	},
	{
		longitude: 119.24217,
		latitude: 26.111457
	},
	{
		longitude: 119.237706,
		latitude: 26.098508
	},
	{
		longitude: 119.227063,
		latitude: 26.0908
	},
	{
		longitude: 119.232213,
		latitude: 26.077541
	},

	{
		longitude: 119.234616,
		latitude: 26.049167
	},
	{
		longitude: 119.244573,
		latitude: 26.025723
	},
	{
		longitude: 119.274099,
		latitude: 26.000422
	},
	{
		longitude: 119.298474,
		latitude: 25.986844
	},
	{
		longitude: 119.342763,
		latitude: 25.993942
	},
]
