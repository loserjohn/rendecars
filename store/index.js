import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
// import http from '../common/http/index.js'
// import SET from '@/SET.js';
import app from './app.js';
import rent from './rent.js';
 
const store = new Vuex.Store({
	modules: {
		app,
		rent
	}
})

export default store