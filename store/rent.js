import http from '../common/http/index.js'
import SET from '@/SET.js';
import Utils from '@/common/utils.js';


let date = new Date()

let mdate = new Date(date.getTime() + 24 * 60 * 60 * 1000)
let s = {
	time: date.getTime(),
	year: date.getFullYear(),
	month: date.getMonth() + 1,
	day: date.getDate(),
	week: date.getDay(),
	text: Utils.parseTime(date.getTime(), '{m}月{d}日')
}
let e = {
	time: mdate.getTime(),
	year: mdate.getFullYear(),
	month: mdate.getMonth() + 1,
	day: mdate.getDate(),
	week: mdate.getDay(),
	text: Utils.parseTime(mdate.getTime(), '{m}月{d}日')
}

const Times = ['8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
	'14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
	'20:00', '20:30'
]

// debugger
const modules = {
	name: 'rent',
	namespaced: true,
	state: {
		rentCar: {}, //选定的车型
		rentDate: [s, e], //用车日期
		rentTime: ['9:00', '15:00'], //取/还车车时间
		rentStartAddress: null, //接车地点
		rentEndAddress: null, //还车地点
		
		autoRentStartAddress:null,  //一键取车地点
		autoRentEndAddress:null,  //一键还车地点
		
		rentArea: {}, //当前的所在城市
		currentOrder: '', //当前订单信息
		location: {}, //当前的经纬度
		hotList: [],
		cityList: [],
		rentAdressList: [] //去还车地址列表
	},

	mutations: {
		// payload为用户传递的值，可以是单一值或者对象
		selectStartAddress(state, payload) {
			state.rentStartAddress = {
				...payload
			}
		},
		selectEndAddress(state, payload) {
			// debugger
			state.rentEndAddress = {
				...payload
			}
			// console.log(state.rentEndAddress )
			// debugger
		},
		selectDate(state, payload) {
			state.rentDate = [...payload]
		},
		selectTime(state, payload) {
			let arr = [Times[payload[0]], Times[payload[1]]]
			state.rentTime = [...arr]
		},

		// 储存经纬度
		saveLocation(state, position) {
			state.location = {
				...position
			}
			// debugger
		},
		// 创建推荐网点
		saveSites(state, payload) {
			state.rentAdressList = [
				...payload
			]
			if (payload[0] && !state.rentStartAddress) {
				state.rentStartAddress = {
					...payload[0]
				}
			} else {
				state.rentStartAddress = null
			}
		},
		// 储存城市列表 和当前所在城市
		saveCity(state, payload) {
			state.hotList = [...payload.hot]
			state.cityList = [...payload.list]
			state.rentArea = {
				...payload.default_area
			}
			if(payload.callback){
				payload.callback()
			}
		},
		// 城市选择
		saveRentCity(state, payload) {
			// debugger
			state.rentArea = {
				...payload
			}

		},
		// 选中车型
		saveRentCar(state, payload) {
			// debugger
			state.rentCar = {
				...payload
			}

		},
		
		//一键取车地址
		save_autoRentStartAddress(state,payload){	
			// debugger
			if(payload){
				state.autoRentStartAddress = {
					...payload
				}
			}else{
				state.autoRentStartAddress = null
			}
			
		},
		
		//一键还车地址
		save_autoRentEndAddress(state,payload){			
			// state.autoRentEndAddress = {
			// 	...payload
			// }
			if(payload){
				state.autoRentEndAddress = {
					...payload
				}
			}else{
				state.autoRentEndAddress = null
			}
		}
	},
	actions: {
		
		//加载城市列表 和默认城市	
		async getCitys({
			state,
			commit,
			dispatch
		}, payload) {
			try {
				let f = {
					...state.location,
					...payload
				}
				let res = await http.Business.GetArea(f);
				// res.callback = ()=>{
				// 
					
				// }
				commit('saveCity', res)
				let area_code = res.default_area.area_code
				dispatch('getServeSite', {
					longitude: payload.longitude,
					latitude: payload.latitude,
					Key: '',
					area_code:area_code
				}) 
			} catch (e) {
				//TODO handle the exception
			}
		},
		// 设置网店里列表  储存 本地经纬度
		async getServeSite({
			state,
			commit
		}, payload) {
			const {
				longitude = '', latitude = '', Key = '',callback=null
			} = payload
			let p = {
				longitude:longitude || state.location.longitude  ,
				latitude:latitude || state.location.latitude  ,
				Key:Key || state.location.Key  
			}
			console.log(payload)
			try {

				let f = {
					// ...state.location,
					area_code: state.rentArea.area_code,
					...p
				}
				
				let res = await http.Business.GetServiceSite(f);
				// console.log(res)
				commit('saveSites',[...res.service_site])
				if(callback)callback()
			} catch (e) {
				//TODO handle the exception
				// debugger
				console.log(e)
			}
		},
		async refreshUser({
			state,
			commit
		}, payload) {
			// console.log(payload);
			let res = await http.getConsumer();
			console.log(res)
			if (res.Success) {
				commit('setUserInfo', res.Data)
			}
		}
	}
}

export default modules
